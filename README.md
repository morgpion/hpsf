# hpsf

Découverte de Symfony via l'univers d'Harry Potter.

# Objectifs

*  base de données simple (2 tables pour pouvoir tester les relations/jointures)
*  test d'un ORM et fake data
*  fonctionnement mVC
*  test d'un moteur de rendu
*  intégration de Bootstrap
*  utilisation des données d'une API REST externe

# Technos

Symfony 5
//...

# Arborescence

//...

# Base de données : MCD - MLD

//...


